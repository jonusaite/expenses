<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Earning extends Model
{
    /**
     * Get earnings category.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\EarningsCategory', 'earnings_category_id');
    }
    
    /**
     * Get user.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
