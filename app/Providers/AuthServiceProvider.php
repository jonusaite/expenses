<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\EarningsCategory' => 'App\Policies\EarningsCategoryPolicy',
        'App\ExpensesCategory' => 'App\Policies\ExpensesCategoryPolicy',
        'App\Earning' => 'App\Policies\EarningPolicy',
        'App\Expense' => 'App\Policies\ExpensePolicy',
    ];
    
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    
        Gate::define('earnings_categories.edit', 'App\Policies\EarningsCategoryPolicy@edit');
        Gate::define('expenses_categories.edit', 'App\Policies\ExpensesCategoryPolicy@edit');
        Gate::define('earnings.edit', 'App\Policies\EarningPolicy@edit');
        Gate::define('expenses.edit', 'App\Policies\ExpensePolicy@edit');
    }
}
