<?php

namespace App\Policies;

use App\User;
use App\Earning;
use Illuminate\Auth\Access\HandlesAuthorization;

class EarningPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view the earning.
     *
     * @param  \App\User $user
     * @param  \App\Earning $earning
     * @return mixed
     */
    public function edit(User $user, Earning $earning)
    {
        return $earning->user_id === $user->id;
    }
}
