<?php

namespace App\Policies;

use App\User;
use App\EarningsCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class EarningsCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the earningsCategory.
     *
     * @param  \App\User  $user
     * @param  \App\EarningsCategory  $earningsCategory
     * @return mixed
     */
    public function edit(User $user, EarningsCategory $earningsCategory)
    {
        return $earningsCategory->user_id === $user->id;
    }
}
