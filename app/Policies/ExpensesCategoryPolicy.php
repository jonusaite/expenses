<?php

namespace App\Policies;

use App\User;
use App\ExpensesCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpensesCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the expensesCategory.
     *
     * @param  \App\User  $user
     * @param  \App\ExpensesCategory  $expensesCategory
     * @return mixed
     */
    public function edit(User $user, ExpensesCategory $expensesCategory)
    {
        return $expensesCategory->user_id === $user->id;
    }
}
