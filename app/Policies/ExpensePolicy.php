<?php

namespace App\Policies;

use App\User;
use App\Expense;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpensePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view the expense.
     *
     * @param  \App\User $user
     * @param  \App\Expense $expense
     * @return mixed
     */
    public function edit(User $user, Expense $expense)
    {
        return $expense->user_id === $user->id;
    }
    
}
