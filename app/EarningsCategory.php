<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EarningsCategory extends Model
{
    /**
     * Get earnings.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function earnings()
    {
        return $this->hasMany('App\Earning');
    }
    
    /**
     * Get earnings budgets.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function budgets()
    {
        return $this->hasMany('App\EarningsBudget');
    }
    
    /**
     * Get user.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
