<?php

namespace App\Http\Controllers;

use App\EarningsCategory;
use App\Expense;
use App\ExpensesCategory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class BalanceController extends Controller
{
    /**
     * Get balance for particular month.
     * @param  Request  $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $where = [];
        $whereBudget = [];
        if ($request->date_from) {
            $where[] = ['date', '>=', $request->date_from];
            $whereBudget[] = ['year', '>=', date('Y', strtotime($request->date_from))];
            $whereBudget[] = ['month', '>=', date('m', strtotime($request->date_from))];
        }
        if ($request->date_to) {
            $where[] = ['date', '<=', $request->date_to];
            $whereBudget[] = ['year', '<=', date('Y', strtotime($request->date_to))];
            $whereBudget[] = ['month', '<=', date('m', strtotime($request->date_to))];
        }

        $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->whereHas('expenses',
            function ($query) use ($where) {
                $query->where('user_id', Auth::user()->id)->where($where);
            })->orWhereHas('budgets',
            function ($query) use ($whereBudget) {
                $query->where([['user_id', Auth::user()->id], ['sum', '>', 0]])->where($whereBudget);
            })->with([
            'expenses' => function ($query) use ($where) {
                $query->where('user_id', Auth::user()->id)->where($where);
            },
            'budgets' => function ($query) use ($whereBudget) {
                $query->where('user_id', Auth::user()->id)->where($whereBudget);
            },
        ])->get();

        $earningsCategories = EarningsCategory::where('user_id', Auth::user()->id)->whereHas('earnings',
            function ($query) use ($where) {
                $query->where('user_id', Auth::user()->id)->where($where);
            })->orWhereHas('budgets',
            function ($query) use ($whereBudget) {
                $query->where([['user_id', Auth::user()->id], ['sum', '>', 0]])->where($whereBudget);
            })->with([
            'earnings' => function ($query) use ($where) {
                $query->where('user_id', Auth::user()->id)->where($where);
            },
            'budgets' => function ($query) use ($whereBudget) {
                $query->where('user_id', Auth::user()->id)->where($whereBudget);
            },
        ])->get();

        $totalEarnings = $earningsCategories->sum(function ($query) {
            return $query['earnings']->sum('sum');
        });

        $totalExpenses = $expensesCategories->sum(function ($query) {
            return $query['expenses']->sum('sum');
        });

        $totalEarningsBudget = $earningsCategories->sum(function ($query) {
            return $query['budgets']->sum('sum');
        });

        $totalExpensesBudget = $expensesCategories->sum(function ($query) {
            return $query['budgets']->sum('sum');
        });

        $diffEarnings = $totalEarnings - $totalEarningsBudget;
        $diffExpenses = $totalExpensesBudget - $totalExpenses;

        $totalDiff = $totalEarnings - $totalExpenses;
        $totalBudgetDiff = $totalEarningsBudget - $totalExpensesBudget;

        return view('balance.index', [
            'expensesCategories' => $expensesCategories,
            'earningsCategories' => $earningsCategories,
            'totalEarnings' => $totalEarnings,
            'totalExpenses' => $totalExpenses,
            'totalEarningsBudget' => $totalEarningsBudget,
            'totalExpensesBudget' => $totalExpensesBudget,
            'diffEarnings' => $diffEarnings,
            'diffExpenses' => $diffExpenses,
            'totalDiff' => $totalDiff,
            'totalBudgetDiff' => $totalBudgetDiff,
            'request' => $request,
        ]);
    }

    /**
     * Overview of current month expenses.
     *
     * @return JsonResponse
     */
    public function overview()
    {
        $year = date('Y');
        $month = date('m');
        $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->whereHas('expenses',
            function ($query) use ($year, $month) {
                $query->where('user_id', Auth::user()->id)->whereBetween('date',
                    [date('Y-m-1', strtotime($year . '-' . $month)), date('Y-m-t', strtotime($year . '-' . $month))]);
            })->with([
            'expenses' => function ($query) use ($year, $month) {
                $query->where('user_id', Auth::user()->id)->whereBetween('date',
                    [date('Y-m-1', strtotime($year . '-' . $month)), date('Y-m-t', strtotime($year . '-' . $month))]);
            },
        ])->get();
        $expenses = [];
        foreach ($expensesCategories as $expensesCategory) {
            $expenses[] = [$expensesCategory->name, $expensesCategory->expenses->sum('sum')];
        }

        return response()->json([
            'expenses' => $expenses,
        ]);
    }
}
