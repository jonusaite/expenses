<?php

namespace App\Http\Controllers;

use App\EarningsBudget;
use App\EarningsCategory;
use App\ExpensesBudget;
use App\ExpensesCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $earningsCategories = EarningsCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();
        $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();
        $year = $request->year ?? date('Y');
        $month = $request->month ?? date('m');
        $earningBudgets = EarningsBudget::where([
            ['user_id', Auth::user()->id],
            ['year', $year],
            ['month', $month],
        ])->get();
        $expensesBudgets = ExpensesBudget::where([
            ['user_id', Auth::user()->id],
            ['year', $year],
            ['month', $month],
        ])->get();
        $months = [
            '01' => 'Sausis', '02' => 'Vasaris', '03' => 'Kovas', '04' => 'Balandis', '05' => 'Gegužė',
            '06' => 'Birželis', '07' => 'Liepa', '08' => 'Rugpjūtis', '09' => 'Rugsėjis', '10' => 'Spalis',
            '11' => 'Lapkritis', '12' => 'Gruodis'
        ];

        return view('budget.index',
            [
                'earningsCategories' => $earningsCategories,
                'expensesCategories' => $expensesCategories,
                'earnings' => $earningBudgets,
                'expenses' => $expensesBudgets,
                'year' => $year,
                'month' => $month,
                'months' => $months,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $earningsCategories = EarningsCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();
        $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();

        return view('budget.create',
            ['earningsCategories' => $earningsCategories, 'expensesCategories' => $expensesCategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'year' => 'required|integer|between:2017,' . (date('Y') + 1),
            'month' => 'required|integer|between:1,12',
            'earnings.*' => 'numeric|between:0,9999999.99',
            'earnings_cat.*' => [
                Rule::exists('earnings_categories', 'id')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
            ],
            'expenses.*' => 'numeric|between:0,9999999.99',
            'expenses_cat.*' => [
                Rule::exists('expenses_categories', 'id')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
            ],
        ]);

        $this->updateOrCreate($request, $request->year, $request->month);

        return redirect()->route('budget.index', ['year' => $request->year, 'month' => $request->month])->with([
            'success' => true,
            'message' => 'Biudžetas sėkmingai sukurtas ' . $request->year . '-' . $request->month,
        ]);
    }

    /**
     * New row in earnings budget.
     * @param $year
     * @param $month
     * @param $earnings_cat
     * @param $sum
     */
    public function newEarningsBudget($year, $month, $earnings_cat, $sum)
    {
        $earningsBudget = new EarningsBudget();
        $earningsBudget->year = $year;
        $earningsBudget->month = $month;
        $earningsBudget->earnings_category_id = $earnings_cat;
        $earningsBudget->sum = $sum;
        $earningsBudget->user_id = Auth::user()->id;
        $earningsBudget->save();
    }

    /**
     * New row in expenses budget.
     * @param $year
     * @param $month
     * @param $expenses_cat
     * @param $sum
     */
    public function newExpensesBudget($year, $month, $expenses_cat, $sum)
    {
        $expensesBudget = new ExpensesBudget();
        $expensesBudget->year = $year;
        $expensesBudget->month = $month;
        $expensesBudget->expenses_category_id = $expenses_cat;
        $expensesBudget->sum = $sum;
        $expensesBudget->user_id = Auth::user()->id;
        $expensesBudget->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $year
     * @param $month
     * @return Response
     */
    public function edit($year, $month)
    {
        $earningsCategories = EarningsCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();
        $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();
        $earningBudgets = EarningsBudget::where([
            ['user_id', Auth::user()->id],
            ['year', $year],
            ['month', $month],
        ])->get();
        $expensesBudgets = ExpensesBudget::where([
            ['user_id', Auth::user()->id],
            ['year', $year],
            ['month', $month],
        ])->get();

        return view('budget.edit', [
            'earningsCategories' => $earningsCategories,
            'expensesCategories' => $expensesCategories,
            'earnings' => $earningBudgets,
            'expenses' => $expensesBudgets,
            'year' => $year,
            'month' => $month,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param $year
     * @param $month
     * @return Response
     */
    public function update(Request $request, $year, $month)
    {
        $request->validate([
            'earnings.*' => 'numeric|between:0,9999999.99',
            'earnings_cat.*' => [
                Rule::exists('earnings_categories', 'id')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
            ],
            'expenses.*' => 'numeric|between:0,9999999.99',
            'expenses_cat.*' => [
                Rule::exists('expenses_categories', 'id')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
            ],
        ]);

        $this->updateOrCreate($request, $year, $month);

        return redirect()->route('budget.index', ['year' => $year, 'month' => $month])->with([
            'success' => true,
            'message' => 'Biudžetas atnaujintas ' . $year . '-' . $month,
        ]);
    }

    /**
     * Update existing budget row or create a new one.
     * @param $request
     * @param $year
     * @param $month
     */
    public function updateOrCreate($request, $year, $month)
    {
        if (isset($request->earnings_cat)) {
            foreach ($request->earnings_cat as $earnings_cat) {
                if (isset($request->earnings[$earnings_cat])) {
                    $earningsBudget = EarningsBudget::where([
                        ['user_id', Auth::user()->id],
                        ['year', $year],
                        ['month', $month],
                        ['earnings_category_id', $earnings_cat],
                    ])->first();
                    if (isset($earningsBudget)) {
                        $earningsBudget->sum = $request->earnings[$earnings_cat];
                        $earningsBudget->save();
                    } else {
                        $this->newEarningsBudget($year, $month, $earnings_cat, $request->earnings[$earnings_cat]);
                    }
                }
            }
        }

        if (isset($request->expenses_cat)) {
            foreach ($request->expenses_cat as $expenses_cat) {
                if (isset($request->expenses[$expenses_cat])) {
                    $expensesBudget = ExpensesBudget::where([
                        ['user_id', Auth::user()->id],
                        ['year', $year],
                        ['month', $month],
                        ['expenses_category_id', $expenses_cat],
                    ])->first();
                    if (isset($expensesBudget)) {
                        $expensesBudget->sum = $request->expenses[$expenses_cat];
                        $expensesBudget->save();
                    } else {
                        $this->newExpensesBudget($year, $month, $expenses_cat, $request->expenses[$expenses_cat]);
                    }
                }
            }
        }
    }

    /**
     * Confirm before deleting budget.
     * @param $year
     * @param $month
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm($year, $month)
    {
        return view('budget.confirm', ['year' => $year, 'month' => $month]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $year
     * @param $month
     * @return Response
     */
    public function destroy($year, $month)
    {
        EarningsBudget::where([
            ['user_id', Auth::user()->id],
            ['year', $year],
            ['month', $month],
        ])->delete();
        ExpensesBudget::where([
            ['user_id', Auth::user()->id],
            ['year', $year],
            ['month', $month],
        ])->delete();

        return redirect()->route('budget.index', [date('Y'), date('m')])->with([
            'success' => true,
            'message' => 'Biudžetas ' . $year . '-' . $month . ' ištrintas.',
        ]);
    }
}
