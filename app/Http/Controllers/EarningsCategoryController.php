<?php

namespace App\Http\Controllers;

use App\EarningsCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class EarningsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $earningsCategories = EarningsCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();
        
        return view('earnings.categories.index', ['categories' => $earningsCategories]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('earnings.categories.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'max:191',
                Rule::unique('earnings_categories')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
            ],
        ]);
        $earningsCategory = new EarningsCategory();
        $earningsCategory->name = $request->name;
        $earningsCategory->user_id = Auth::user()->id;
        $earningsCategory->save();
        
        return redirect()->back()->with([
            'success' => true,
            'message' => 'Kategorija &#0132;' . $earningsCategory->name . '&#0147; pridėta sėkmingai.',
        ]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EarningsCategory $earningsCategory
     * @return Response
     */
    public function edit(EarningsCategory $earningsCategory)
    {
        if (Gate::allows('earnings_categories.edit', $earningsCategory)) {
            return view('earnings.categories.edit', ['category' => $earningsCategory]);
        } else {
            abort(401, 'Kategorija nepriklauso vartotojui.');
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  \App\EarningsCategory $earningsCategory
     * @return Response
     */
    public function update(Request $request, EarningsCategory $earningsCategory)
    {
        if (Gate::allows('earnings_categories.edit', $earningsCategory)) {
            $request->validate([
                'name' => [
                    'required',
                    'max:191',
                    Rule::unique('earnings_categories')->where(function ($query) use ($earningsCategory) {
                        $query->where([
                            ['user_id', Auth::user()->id],
                            ['id', '!=', $earningsCategory->id],
                        ]);
                    }),
                ],
            ]);
            $earningsCategory->name = $request->name;
            $earningsCategory->save();
            
            return redirect()->back()->with([
                'success' => true,
                'message' => 'Kategorija &#0132;' . $earningsCategory->name . '&#0147; atnaujinta sėkmingai.',
            ]);
        } else {
            abort(401, 'Kategorija nepriklauso vartotojui.');
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EarningsCategory $earningsCategory
     * @return Response
     * @throws \Exception
     */
    public function destroy(EarningsCategory $earningsCategory)
    {
        if (Gate::allows('earnings_categories.edit', $earningsCategory)) {
            if ($earningsCategory->earnings->count() !== 0) {
                return redirect()->back()->with([
                    'success' => false,
                    'message' => 'Negalima ištrinti kategorijos, kuri turi pajamų.',
                ]);
            } elseif ($earningsCategory->budgets->count() !== 0) {
                return redirect()->back()->with([
                    'success' => false,
                    'message' => 'Negalima ištrinti kategorijos, kuri turi biudžetą.',
                ]);
            } else {
                $earningsCategory->delete();
                
                return redirect()->back()->with([
                    'success' => true,
                    'message' => 'Kategorija &#0132;' . $earningsCategory->name . '&#0147; ištrinta.',
                ]);
            }
        } else {
            abort(401, 'Kategorija nepriklauso vartotojui.');
        }
    }
}
