<?php

namespace App\Http\Controllers;

use App\ExpensesCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class ExpensesCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();
        
        return view('expenses.categories.index', ['categories' => $expensesCategories]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('expenses.categories.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'max:191',
                Rule::unique('expenses_categories')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
            ],
        ]);
        $expensesCategory = new ExpensesCategory();
        $expensesCategory->name = $request->name;
        $expensesCategory->user_id = Auth::user()->id;
        $expensesCategory->save();
        
        return redirect()->back()->with([
            'success' => true,
            'message' => 'Kategorija &#0132;' . $expensesCategory->name . '&#0147; pridėta sėkmingai.',
        ]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpensesCategory $expensesCategory
     * @return Response
     */
    public function edit(ExpensesCategory $expensesCategory)
    {
        if (Gate::allows('expenses_categories.edit', $expensesCategory)) {
            return view('expenses.categories.edit', ['category' => $expensesCategory]);
        } else {
            abort(401, 'Kategorija nepriklauso vartotojui.');
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  \App\ExpensesCategory $expensesCategory
     * @return Response
     */
    public function update(Request $request, ExpensesCategory $expensesCategory)
    {
        if (Gate::allows('expenses_categories.edit', $expensesCategory)) {
            $request->validate([
                'name' => [
                    'required',
                    'max:191',
                    Rule::unique('expenses_categories')->where(function ($query) use ($expensesCategory) {
                        $query->where([
                            ['user_id', Auth::user()->id],
                            ['id', '!=', $expensesCategory->id],
                        ]);
                    }),
                ],
            ]);
            $expensesCategory->name = $request->name;
            $expensesCategory->save();
            
            return redirect()->back()->with([
                'success' => true,
                'message' => 'Kategorija &#0132;' . $expensesCategory->name . '&#0147; atnaujinta sėkmingai.',
            ]);
        } else {
            abort(401, 'Kategorija nepriklauso vartotojui.');
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpensesCategory $expensesCategory
     * @return Response
     * @throws \Exception
     */
    public function destroy(ExpensesCategory $expensesCategory)
    {
        if (Gate::allows('expenses_categories.edit', $expensesCategory)) {
            if ($expensesCategory->expenses->count() !== 0) {
                return redirect()->back()->with([
                    'success' => false,
                    'message' => 'Negalima ištrinti kategorijos, kuri turi išlaidų.',
                ]);
            } elseif ($expensesCategory->budgets->count() !== 0) {
                return redirect()->back()->with([
                    'success' => false,
                    'message' => 'Negalima ištrinti kategorijos, kuri turi biudžetą.',
                ]);
            } else {
                $expensesCategory->delete();
                
                return redirect()->back()->with([
                    'success' => true,
                    'message' => 'Kategorija &#0132;' . $expensesCategory->name . '&#0147; ištrinta.',
                ]);
            }
        } else {
            abort(401, 'Kategorija nepriklauso vartotojui.');
        }
    }
}
