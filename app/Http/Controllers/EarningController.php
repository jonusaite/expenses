<?php

namespace App\Http\Controllers;

use App\Earning;
use App\EarningsCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class EarningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $where = [];
        if ($request->date_from) {
            $where[] = ['date', '>=', $request->date_from];
        }
        if ($request->date_to) {
            $where[] = ['date', '<=', $request->date_to];
        }
        if ($request->category) {
            $where[] = ['earnings_category_id', $request->category];
        }
        $earnings = Earning::where('user_id', Auth::user()->id)->where($where)->orderBy('date', 'desc')
                           ->orderBy('created_at', 'desc')->with('category')->paginate(20);
        $categories = EarningsCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();

        return view('earnings.index', ['earnings' => $earnings, 'categories' => $categories, 'request' => $request]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $earningsCategories = EarningsCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();

        return view('earnings.create', ['earningsCategories' => $earningsCategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|date|after:2016-12-31',
            'category' => Rule::exists('earnings_categories', 'id')->where(function ($query) {
                $query->where('user_id', Auth::user()->id);
            }),
            'sum' => 'required|numeric|between:0,9999999.99',
            'comment' => 'nullable|string|max:191',
        ]);
        $earning = new Earning();
        $earning->date = $request->date;
        $earning->earnings_category_id = $request->category;
        $earning->sum = $request->sum;
        $earning->comment = $request->comment;
        $earning->user_id = Auth::user()->id;
        $earning->save();

        return redirect()->back()->with([
            'success' => true,
            'message' => 'Pajamos sėkmingai pridėtos',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Earning  $earning
     * @return Response
     */
    public function edit(Earning $earning)
    {
        if (Gate::allows('earnings.edit', $earning)) {
            $earningsCategories = EarningsCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();

            return view('earnings.edit', ['earning' => $earning, 'earningsCategories' => $earningsCategories]);
        } else {
            abort(401, 'Šios pajamos nepriklauso vartotojui.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  \App\Earning  $earning
     * @return Response
     */
    public function update(Request $request, Earning $earning)
    {
        if (Gate::allows('earnings.edit', $earning)) {
            $request->validate([
                'date' => 'required|date|after:2016-12-31',
                'category' => Rule::exists('earnings_categories', 'id')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
                'sum' => 'required|numeric|between:0,9999999.99',
                'comment' => 'nullable|string|max:191',
            ]);
            $earning->date = $request->date;
            $earning->earnings_category_id = $request->category;
            $earning->sum = $request->sum;
            $earning->comment = $request->comment;
            $earning->save();

            return redirect()->route('earnings.index',
                [date('Y', strtotime($earning->date)), date('m', strtotime($earning->date))])->with([
                'success' => true,
                'message' => 'Pajamos sėkmingai atnaujintos',
            ]);

        } else {
            abort(401, 'Šios pajamos nepriklauso vartotojui.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Earning  $earning
     * @return Response
     * @throws \Exception
     */
    public function destroy(Earning $earning)
    {
        if (Gate::allows('earnings.edit', $earning)) {
            $earning->delete();

            return redirect()->back()->with([
                'success' => true,
                'message' => 'Pajamos ' . $earning->date . ' ' . $earning->category->name . ', suma: ' . $earning->sum .
                             '  ištrintos.',
            ]);
        } else {
            abort(401, 'Šios pajamos nepriklauso vartotojui.');
        }
    }

}
