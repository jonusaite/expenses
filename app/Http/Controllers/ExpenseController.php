<?php

namespace App\Http\Controllers;

use App\Expense;
use App\ExpensesCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $where = [];
        if ($request->date_from) {
            $where[] = ['date', '>=', $request->date_from];
        }
        if ($request->date_to) {
            $where[] = ['date', '<=', $request->date_to];
        }
        if ($request->category) {
            $where[] = ['expenses_category_id', $request->category];
        }
        $expenses = Expense::where('user_id', Auth::user()->id)->where($where)->orderBy('date', 'desc')
                           ->orderBy('created_at', 'desc')->with('category')->paginate(20);
        $categories = ExpensesCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();

        return view('expenses.index', ['expenses' => $expenses, 'categories' => $categories, 'request' => $request]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();

        return view('expenses.create', ['expensesCategories' => $expensesCategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|date|after:2016-12-31',
            'category' => Rule::exists('expenses_categories', 'id')->where(function ($query) {
                $query->where('user_id', Auth::user()->id);
            }),
            'sum' => 'required|numeric|between:0,9999999.99',
            'comment' => 'nullable|string|max:191',
        ]);
        $expense = new Expense();
        $expense->date = $request->date;
        $expense->expenses_category_id = $request->category;
        $expense->sum = $request->sum;
        $expense->comment = $request->comment;
        $expense->user_id = Auth::user()->id;
        $expense->save();

        return redirect()->back()->with([
            'success' => true,
            'message' => 'Išlaidos sėkmingai pridėtos',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return Response
     */
    public function edit(Expense $expense)
    {
        if (Gate::allows('expenses.edit', $expense)) {
            $expensesCategories = ExpensesCategory::where('user_id', Auth::user()->id)->orderBy('name', 'asc')->get();

            return view('expenses.edit', ['expense' => $expense, 'expensesCategories' => $expensesCategories]);
        } else {
            abort(401, 'Šios išlaidos nepriklauso vartotojui.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  \App\Expense  $expense
     * @return Response
     */
    public function update(Request $request, Expense $expense)
    {
        if (Gate::allows('expenses.edit', $expense)) {
            $request->validate([
                'date' => 'required|date|after:2016-12-31',
                'category' => Rule::exists('expenses_categories', 'id')->where(function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }),
                'sum' => 'required|numeric|between:0,9999999.99',
                'comment' => 'nullable|string|max:191',
            ]);
            $expense->date = $request->date;
            $expense->expenses_category_id = $request->category;
            $expense->sum = $request->sum;
            $expense->comment = $request->comment;
            $expense->save();

            return redirect()->route('expenses.index',
                [date('Y', strtotime($expense->date)), date('m', strtotime($expense->date))])->with([
                'success' => true,
                'message' => 'Išlaidos sėkmingai atnaujintos',
            ]);
        } else {
            abort(401, 'Šios išlaidos nepriklauso vartotojui.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return Response
     * @throws \Exception
     */
    public function destroy(Expense $expense)
    {
        if (Gate::allows('expenses.edit', $expense)) {
            $expense->delete();

            return redirect()->back()->with([
                'success' => true,
                'message' => 'Išlaidos ' . $expense->date . ' ' . $expense->category->name . ', suma: ' .
                             $expense->sum . ' ištrintos.',
            ]);
        } else {
            abort(401, 'Šios išlaidos nepriklauso vartotojui.');
        }
    }

}
