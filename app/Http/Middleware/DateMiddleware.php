<?php

namespace App\Http\Middleware;

use Closure;

class DateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->year && !$request->month) {
            return redirect()->route($request->route()->getName(), ['year' => date('Y'), 'month' => date('m')]);
        }
        if (preg_match('/[0-9]+/', $request->year) && preg_match('/[0-9]+/',
                $request->month) && $request->year >= 2017 && $request->year < date('Y') + 1 && checkdate($request->month,
                1, $request->year)) {
            return $next($request);
        } else {
            abort(404, 'Netinkama data: ' . $request->year . '-' . $request->month);
        }
    }
}
