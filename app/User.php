<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Get earnings.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function earnings()
    {
        return $this->hasMany('App\Earnings');
    }
    
    /**
     * Get earnings budgets.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function earningBudgets()
    {
        return $this->hasMany('App\EarningsBudget');
    }
    
    /**
     * Get earnings categories.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function earningsCategories()
    {
        return $this->hasMany('App\EarningsCategory');
    }
    
    /**
     * Get expenses.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses()
    {
        return $this->hasMany('App\Expense');
    }
    
    /**
     * Get expeses budgets.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expensesBudgets()
    {
        return $this->hasMany('App\ExpensesBudget');
    }
    
    /**
     * Get expenses categories.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expensesCategories()
    {
        return $this->hasMany('App\ExpensesCategory');
    }
}
