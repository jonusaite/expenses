<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('year');
            $table->smallInteger('month');
            $table->unsignedInteger('expenses_category_id');
            $table->float('sum', 9, 2);
            $table->unsignedInteger('user_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('expenses_category_id')->references('id')->on('expenses_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_budgets');
    }
}
