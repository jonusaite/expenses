<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarningsBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earnings_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('year');
            $table->smallInteger('month');
            $table->unsignedInteger('earnings_category_id');
            $table->float('sum', 9, 2);
            $table->unsignedInteger('user_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('earnings_category_id')->references('id')->on('earnings_categories');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earnings_budgets');
    }
}
