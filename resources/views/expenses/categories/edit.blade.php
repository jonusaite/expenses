@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success') !== null)
            <div class="row justify-content-md-center">
                <div class="col-md-8">

                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                </div>
            </div>
        @endif
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-light">Redaguoti išlaidų kategoriją</div>
                    <div class="card-body">
                        <form role="form" method="POST"
                              action="{{ route('expenses_categories.update', $category->id) }}">
                            {{ method_field('PUT') }}
                            {!! csrf_field() !!}

                            <div class="form-group row">
                                <label for="name" class="col-lg-4 col-form-label text-lg-right">Pavadinimas</label>

                                <div class="col-lg-6">
                                    <input id="name"
                                           type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name"
                                           value="{{ old('name') ? old('name') : $category->name }}"
                                           autofocus
                                           required
                                    >
                                    @if ($errors->has('name'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-6 offset-lg-4">
                                    <button type="submit" class="btn btn-dark">
                                        Atnaujinti
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
