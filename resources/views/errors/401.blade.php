@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card border-secondary">
                    <div class="card-header bg-secondary text-light">Neautorizuotas veiksmas</div>
                    <div class="card-body">
                        {{ $exception->getMessage() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
