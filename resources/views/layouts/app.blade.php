<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Aistė Jonušaitė">
    <meta name="description" content="Pajamų ir išlaidų skaičiuoklė">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>
</head>
<body>
<div id="app">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                Apžvalga
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                @if (!Auth::guest())
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                Biudžetas
                            </a>
                            <div class="dropdown-menu">
                                <a href="{{ route('budget.index', [date('Y'), date('m')]) }}" class="dropdown-item">
                                    Biudžetas
                                </a>
                                <a href="{{ route('budget.create') }}" class="dropdown-item">
                                    Sukurti naują biudžetą
                                </a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                Pajamų kategorijos
                            </a>
                            <div class="dropdown-menu">
                                <a href="{{ route('earnings_categories.index') }}" class="dropdown-item">
                                    Pajamų kategorijos
                                </a>
                                <a href="{{ route('earnings_categories.create') }}" class="dropdown-item">
                                    Sukurti naują kategoriją
                                </a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                Išlaidų kategorijos
                            </a>
                            <div class="dropdown-menu">
                                <a href="{{ route('expenses_categories.index') }}" class="dropdown-item">
                                    Išlaidų kategorijos
                                </a>
                                <a href="{{ route('expenses_categories.create') }}" class="dropdown-item">
                                    Sukurti naują kategoriją
                                </a>
                            </div>
                        </li>


                        <li>
                            <a class="nav-link" href="{{ route('balance.index', [date('Y'), date('m')]) }}">Balansas</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('earnings.index', [date('Y'), date('m')]) }}">Pajamos</a>
                        </li>
                        <li>
                            <a class="nav-link"
                               href="{{ route('expenses.index', [date('Y'), date('m')]) }}">Išlaidos</a>
                        </li>
                    </ul>
                @endif
                <ul class="navbar-nav">
                    @if (Auth::guest())
                        <li class="nav-item"><a href="{{ route('login') }}" class="nav-link">Prisijungti</a></li>
                        <li class="nav-item"><a href="{{ route('register') }}" class="nav-link">Registruotis</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a href="{{ route('logout') }}" class="dropdown-item"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Atsijungti
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>

        </div>
    </nav>

    @if (Auth::user())
        <div class="container text-right mb-1">
            <a href="{{ route('earnings.create') }}" class="btn btn-dark">Pridėti pajamas</a>
            <a href="{{ route('expenses.create') }}" class="btn btn-dark">Pridėti išlaidas</a>
        </div>
    @endif

    @yield('content')
</div>

<footer class="navbar fixed-bottom navbar-dark bg-dark justify-content-center">
    <span class="navbar-text">&copy; 2018 <a href="http://aiste.kuriu.eu" target="_blank">Aistė Jonušaitė</a></span>
</footer>

<!-- Scripts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="{{ asset(mix('js/app.js')) }}"></script>
</body>
</html>
