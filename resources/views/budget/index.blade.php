@extends('layouts.app')

@section('content')

    <div class="container mb-3">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-md-8 mb-lg-0 mb-md-0 mb-xl-0">
                        @if (session('success') !== null)
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                                    {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @else
                                <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                                    Klaida! {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        @endif
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="container mb-3">
        <div class="card border-dark">
            <div class="card-header bg-dark text-light">
                <div class="row">
                    <div class="col">
                        Biudžetas {{ $year }}-{{ $month }}
                        <i class="fas fa-question-circle" data-toggle="popover" data-placement="right"
                           data-content="Šioje skiltyje galite suplanuoti savo kiekvieno mėnesio pajamas ir išlaidas."></i>
                    </div>
                    <div class="col text-right">
                        <a href="{{ route('budget.edit', [$year, $month]) }}"
                           class="btn btn-sm btn-success text-light"><span class="fas fa-pencil-alt"></span></a>
                        <a href="{{ route('budget.confirm', [$year, $month]) }}"
                           class="btn btn-sm btn-danger text-light"><span class="fas fa-trash-alt"></span></a>

                        <button class="btn btn-sm btn-light text-dark" type="button" data-toggle="collapse"
                                data-target="#dates" aria-expanded="false" aria-controls="dates">Filtras</button>
                    </div>
                </div>
            </div>
            <div id="dates" class="collapse">
                <div class="card-body">
                    <div class="row">
                        <form class="form-inline" method="GET" action="{{ route('budget.index') }}">
                            <label for="year" class="sr-only"></label>
                            <input id="year" type="number" name="year" class="form-control mr-2"
                                   value="{{ $year }}" placeholder="Metai">

                            <label for="month" class="sr-only"></label>
                            <select id="month" name="month" class="form-control mr-2">
                                <option value="">-- Mėnuo --</option>
                                @foreach ($months as $number => $name)
                                    <option value="{{ $number }}"
                                            {{ $month == $number ? 'selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>

                            <button type="submit" class="btn btn-primary">Filtruoti</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-6 mb-3">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-light">Pajamos</div>
                    <div class="card-body">

                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th>Pajamų kategorija</th>
                                <th class="text-right">Suma</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($earningsCategories as $earningsCategory)
                                <tr>
                                    <td>{{ $earningsCategory->name }}</td>
                                    <td class="text-right">{{ number_format($earnings->where('earnings_category_id', $earningsCategory->id)->sum('sum'), 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr class="table-success">
                                <td>Iš viso</td>
                                <td class="text-right">{{ number_format($earnings->sum('sum'), 2) }}</td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-light">Išlaidos</div>
                    <div class="card-body">

                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th>Išlaidų kategorija</th>
                                <th class="text-right">Suma</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($expensesCategories as $expensesCategory)
                                <tr>
                                    <td>{{ $expensesCategory->name }}</td>
                                    <td class="text-right">{{ number_format($expenses->where('expenses_category_id', $expensesCategory->id)->sum('sum'), 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr class="table-danger">
                                <td>Iš viso</td>
                                <td class="text-right">{{ number_format($expenses->sum('sum'), 2) }}</td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
