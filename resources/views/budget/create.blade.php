@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-light">Sukurti mėnesio biudžetą</div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('budget.store') }}">
                            {!! csrf_field() !!}
                            <div class="form-group row">
                                <label for="year" class="col-md-2 col-form-label text-lg-right">Metai</label>

                                <div class="col-md-4">
                                    <select id="year"
                                            class="form-control{{ $errors->has('year') ? ' is-invalid' : '' }}"
                                            name="year" required>
                                        <option value="{{ date('Y') - 1 }}"{{ old('year') === date('Y') - 1 ? ' selected' : '' }}>{{ date('Y') - 1 }}</option>
                                        <option value="{{ date('Y') }}"{{ old('year') ? (old('year') === date('Y') ? ' selected' : '') : ' selected' }}>{{ date('Y') }}</option>
                                        <option value="{{ date('Y') + 1 }}"{{ old('year') === date('Y') + 1 ? ' selected' : '' }}>{{ date('Y') + 1 }}</option>
                                    </select>
                                    @if ($errors->has('year'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <label for="month" class="col-md-2 col-form-label text-lg-right">Mėnuo</label>

                                <div class="col-md-4">
                                    <select id="month"
                                            class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            name="month"
                                            required
                                    >
                                        @for ($i = 1; $i <= 12; $i++)
                                            <option value="{{ $i }}"{{ (int)$i === (int)date('n') ? ' selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                    @if ($errors->has('month'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('month') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            @if ($earningsCategories->count() > 0)
                                <h4>Pajamos</h4>
                                @foreach ($earningsCategories as $earningsCategory)
                                    <input type="hidden" name="earnings_cat[{{ $earningsCategory->id }}]"
                                           value="{{ $earningsCategory->id }}">
                                    <div class="form-group row">
                                        <label for="earnings[{{ $earningsCategory->id }}]"
                                               class="col-lg-4 col-form-label text-lg-right">{{ $earningsCategory->name }}</label>

                                        <div class="col-lg-6">
                                            <input id="earnings[{{ $earningsCategory->id }}]"
                                                   type="text"
                                                   class="form-control{{ $errors->has("earnings.{$earningsCategory->id}") ? ' is-invalid' : '' }}"
                                                   name="earnings[{{ $earningsCategory->id }}]"
                                                   value="{{ old("earnings.{$earningsCategory->id}") ? old("earnings.{$earningsCategory->id}") : 0 }}"
                                                   autofocus
                                            >
                                            @if ($errors->has("earnings.{$earningsCategory->id}"))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first("earnings.{$earningsCategory->id}") }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                            @if ($expensesCategories->count() > 0)
                                <h4>Išlaidos</h4>
                                @foreach ($expensesCategories as $expensesCategory)
                                    <input type="hidden" name="expenses_cat[{{ $expensesCategory->id }}]"
                                           value="{{ $expensesCategory->id }}">
                                    <div class="form-group row">
                                        <label for="expenses[{{ $expensesCategory->id }}]"
                                               class="col-lg-4 col-form-label text-lg-right">{{ $expensesCategory->name }}</label>

                                        <div class="col-lg-6">
                                            <input id="expenses[{{ $expensesCategory->id }}]"
                                                   type="text"
                                                   class="form-control{{ $errors->has("expenses.{$expensesCategory->id}") ? ' is-invalid' : '' }}"
                                                   name="expenses[{{ $expensesCategory->id }}]"
                                                   value="{{ old("expenses.{$expensesCategory->id}") ? old("expenses.{$expensesCategory->id}") : 0 }}"
                                                   autofocus
                                            >
                                            @if ($errors->has("expenses.{$expensesCategory->id}"))
                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->first("expenses.{$expensesCategory->id}") }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                            <div class="form-group row">
                                <div class="col-lg-6 offset-lg-4">
                                    <button type="submit" class="btn btn-dark">
                                        Pridėti
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
