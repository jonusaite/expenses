@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-light">Panaikinti biudžetą?</div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('budget.destroy', [$year, $month]) }}">
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}

                            Ar tikrai norite panaikinti {{ $year }}-{{ $month }} biudžetą?

                            <button type="submit" class="btn btn-danger">
                                Panaikinti
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
