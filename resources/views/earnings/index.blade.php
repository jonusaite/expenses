@extends('layouts.app')

@section('content')

    <div class="container mb-3">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-md-8 mb-lg-0 mb-md-0 mb-xl-0">
                        @if (session('success') !== null)
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                                    {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @else
                                <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                                    Klaida! {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        @endif
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="container mb-3">
        <div class="card border-dark">
            <div class="card-header bg-dark text-light">
                <div class="row">
                    <div class="col">Pajamos {{ $request->date_from }} - {{ $request->date_to }}
                        <i class="fas fa-question-circle" data-toggle="popover" data-placement="right"
                           data-content="Šioje skiltyje pateikiamos Jūsų pasirinkto laikotarpio pajamos."></i>
                    </div>
                    <div class="col text-right">
                        <button class="btn btn-sm btn-light text-dark" type="button" data-toggle="collapse"
                                data-target="#dates" aria-expanded="false" aria-controls="dates">Filtras</button>
                    </div>
                </div>
            </div>
            <div id="dates" class="collapse">
                <div class="card-body">
                    <div class="row">
                        <form class="form-inline" method="GET" action="{{ route('earnings.index') }}">
                            <label for="date_from" class="sr-only"></label>
                            <input id="date_from" name="date_from" class="form-control datepicker mr-2"
                                   value="{{ $request->date_from }}" placeholder="Data nuo">

                            <label for="date_to" class="sr-only"></label>
                            <input id="date_to" name="date_to" class="form-control datepicker mr-2"
                                   value="{{ $request->date_to }}" placeholder="Data iki">

                            <label for="category" class="sr-only"></label>
                            <select id="category" name="category" class="form-control mr-2">
                                <option value="">-- Kategorija --</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                            {{ $request->category == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>

                            <button type="submit" class="btn btn-primary">Filtruoti</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="card border-dark">
            <div class="table-responsive">

                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>Data</th>
                        <th>Kategorija</th>
                        <th class="text-right">Suma</th>
                        <th>Komentaras</th>
                        <th>Redaguoti</th>
                        <th>Ištrinti</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($earnings as $earning)
                        <tr>
                            <td>{{ $earning->date }}</td>
                            <td>{{ $earning->category ? $earning->category->name : '' }}</td>
                            <td class="text-right">{{ number_format($earning->sum, 2) }}</td>
                            <td>{{ $earning->comment }}</td>
                            <td><a href="{{ route('earnings.edit', $earning->id) }}"
                                   class="btn btn-sm btn-success text-light"><span class="fas fa-pencil-alt"></span></a>
                            </td>
                            <td>
                                <form method="post"
                                      action="{{ route('earnings.destroy', $earning->id) }}">
                                    {{ method_field('DELETE') }}
                                    {!! csrf_field() !!}
                                    <button type="submit"
                                            class="btn btn-danger btn-sm">
                                        <span class="fas fa-trash-alt"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                {{ $earnings->appends(['date_from' => $request->date_from, 'date_to' => $request->date_to,
                        'category' => $request->category])->links() }}

            </div>
        </div>
    </div>
@endsection
