@extends('layouts.app')

@section('content')

    <div class="container">
        @if (session('success'))
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-light">Pridėti pajamas</div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('earnings.store') }}">
                            {!! csrf_field() !!}

                            <div class="form-group row">
                                <label for="date" class="col-lg-4 col-form-label text-lg-right">Data</label>

                                <div class="col-lg-6">
                                    <input id="date"
                                           type="text"
                                           class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} datepicker"
                                           name="date"
                                           value="{{ old('date') ? old('date') : date('Y-m-d') }}"
                                           required
                                    >
                                    @if ($errors->has('date'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('date') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="category"
                                       class="col-lg-4 col-form-label text-lg-right">Kategorija</label>
                                <div class="col-lg-6">
                                    <select id="category" class="custom-select form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" name="category" required>
                                        <option value="">-- Pasirinkite --</option>
                                        @foreach ($earningsCategories as $earningsCategory)
                                            <option value="{{ $earningsCategory->id }}"{{ (int)old('category') === (int)$earningsCategory->id ? ' selected' : '' }}>{{ $earningsCategory->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('category') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sum" class="col-lg-4 col-form-label text-lg-right">Suma</label>

                                <div class="col-lg-6">
                                    <input id="sum"
                                           type="text"
                                           class="form-control{{ $errors->has('sum') ? ' is-invalid' : '' }}"
                                           name="sum"
                                           value="{{ old('sum') }}"
                                           required
                                    >
                                    @if ($errors->has('sum'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('sum') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="comment" class="col-lg-4 col-form-label text-lg-right">Komentaras</label>

                                <div class="col-lg-6">
                                    <input id="comment"
                                           type="text"
                                           class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}"
                                           name="comment"
                                           value="{{ old('comment') }}"
                                    >
                                    @if ($errors->has('comment'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('comment') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-6 offset-lg-4">
                                    <button type="submit" class="btn btn-dark">
                                        Pridėti
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
