@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="row">

                    <div class="col-md-8">
                        @if (session('success') !== null)
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                                    {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @else
                                <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                                    Klaida! {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        @endif
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-light">Pajamų kategorijos
                        <i class="fas fa-question-circle" data-toggle="popover" data-placement="right"
                           data-content="Šioje skiltyje pateikiamas Jūsų pajamų kategorijų sąrašas."></i>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead>
                            <tr>
                                <th>Pavadinimas</th>
                                <th>Redaguoti</th>
                                <th>Ištrinti</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td>
                                        <a href="{{ route('earnings_categories.edit', $category->id) }}"
                                           class="btn btn-primary btn-sm">
                                            <span class="fas fa-pencil-alt"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <form method="post"
                                              action="{{ route('earnings_categories.destroy', $category->id) }}">
                                            {{ method_field('DELETE') }}
                                            {!! csrf_field() !!}
                                            <button type="submit"
                                                    class="btn btn-danger btn-sm">
                                                <span class="fas fa-trash-alt"></span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
