@extends('layouts.app')

@section('content')

    <div class="container mt-2">
        <div class="card border-dark">
            <div class="card-header bg-dark text-light">Šio mėnesio apžvalga
                <i class="fas fa-question-circle" data-toggle="popover" data-placement="right"
                   data-content="Šioje skiltyje pateikiama skritulinė diagrama su išlaidomis."></i>
            </div>

            <div class="card-body table-responsive">

                <chart></chart>

            </div>
        </div>
    </div>

@endsection
