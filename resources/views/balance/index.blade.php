@extends('layouts.app')

@section('content')

    <div class="container mb-3">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-md-8 mb-lg-0 mb-md-0 mb-xl-0">
                        @if (session('success') !== null)
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                                    {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @else
                                <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                                    Klaida! {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        @endif
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div class="container mb-3">
        <div class="card border-dark">
            <div class="card-header bg-dark text-light">
                <div class="row">
                    <div class="col">Balansas {{ $request->date_from }} - {{ $request->date_to }}
                        <i class="fas fa-question-circle" data-toggle="popover" data-placement="right"
                           data-content="Šioje skiltyje pateikiamas pasirinkto laikotarpio skirtumas tarp biudžeto ir realių pajamų bei išlaidų."></i>
                    </div>
                    <div class="col text-right">
                        <button class="btn btn-sm btn-light text-dark" type="button" data-toggle="collapse"
                                data-target="#dates" aria-expanded="false" aria-controls="dates">Filtras
                        </button>
                    </div>
                </div>
            </div>
            <div id="dates" class="collapse">
                <div class="card-body">
                    <div class="row">
                        <form class="form-inline" method="GET" action="{{ route('balance.index') }}">
                            <label for="date_from" class="sr-only"></label>
                            <input id="date_from" name="date_from" class="form-control datepicker mr-2"
                                   value="{{ $request->date_from }}" placeholder="Data nuo">

                            <label for="date_to" class="sr-only"></label>
                            <input id="date_to" name="date_to" class="form-control datepicker mr-2"
                                   value="{{ $request->date_to }}" placeholder="Data iki">

                            <button type="submit" class="btn btn-primary">Filtruoti</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-2">
        <div class="card border-secondary">
            <div class="table-responsive">

                <table class="table table-hover table-sm">
                    <thead class="thead-light">
                    <tr>
                        <th>Kategorija</th>
                        <th class="text-right">Iš viso</th>
                        <th class="text-right">Biudžetas</th>
                        <th class="text-right">Skirtumas</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th class="text-center" colspan="4">Pajamos</th>
                    </tr>
                    @foreach ($earningsCategories as $earningsCategory)
                        <tr>
                            <th>{{ $earningsCategory->name }}</th>
                            <td class="table-primary text-right">{{ number_format($earningsCategory->earnings->sum('sum'), 2) }}</td>
                            <td class="text-right">{{ number_format($earningsCategory->budgets->sum('sum'), 2) }}</td>
                            <td class="text-right {{ $earningsCategory->earnings->sum('sum') - $earningsCategory->budgets->sum('sum') < 0 ? 'table-danger' : 'table-success' }}"
                                data-toggle="tooltip" data-placement="top"
                                title="{{ $earningsCategory->budgets->sum('sum') > 0 ? round(($earningsCategory->budgets->sum('sum') - $earningsCategory->earnings->sum('sum')) * 100 / $earningsCategory->budgets->sum('sum'), 2) : 0 }}%">
                                {{ number_format(($earningsCategory->earnings->sum('sum') - $earningsCategory->budgets->sum('sum')), 2) }}
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <th>Iš viso</th>
                        <td class="table-primary text-right">{{ number_format($totalEarnings, 2) }}</td>
                        <td class="text-right">{{ number_format($totalEarningsBudget, 2) }}</td>
                        <td class="text-right {{ $diffEarnings < 0 ? 'table-danger' : 'table-success' }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="{{ $totalEarningsBudget > 0 ? round($diffEarnings * 100 / $totalEarningsBudget, 2) : 0 }}%">{{ number_format($diffEarnings, 2) }}</td>
                    </tr>
                    <tr>
                        <th class="text-center" colspan="4">Išlaidos</th>
                    </tr>
                    @foreach ($expensesCategories as $expensesCategory)
                        <tr>
                            <th>{{ $expensesCategory->name }}</th>
                            <td class="table-primary text-right">{{ number_format($expensesCategory->expenses->sum('sum'), 2) }}</td>
                            <td class="text-right">{{ number_format($expensesCategory->budgets->sum('sum'), 2) }}</td>
                            <td class="text-right {{ $expensesCategory->budgets->sum('sum') - $expensesCategory->expenses->sum('sum') < 0 ? 'table-danger' : 'table-success' }}"
                                data-toggle="tooltip" data-placement="top"
                                title="Liko {{ $expensesCategory->budgets->sum('sum') > 0 ? round(($expensesCategory->budgets->sum('sum') - $expensesCategory->expenses->sum('sum')) * 100 / $expensesCategory->budgets->sum('sum'), 2) : 0 }}%">
                                {{ number_format(($expensesCategory->budgets->sum('sum') - $expensesCategory->expenses->sum('sum')), 2) }}
                            </td>
                        </tr>
                    @endforeach

                    <tr>
                        <th>Iš viso</th>
                        <td class="table-primary text-right">{{ number_format($totalExpenses, 2) }}</td>
                        <td class="text-right">{{ number_format($totalExpensesBudget, 2) }}</td>
                        <td class="text-right {{ $diffExpenses < 0 ? 'table-danger' : 'table-success' }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="{{ $totalExpensesBudget > 0 ? round($diffExpenses * 100 / $totalExpensesBudget, 2) : 0 }}%">{{ number_format($diffExpenses, 2) }}</td>
                    </tr>

                    <tr>
                        <th class="text-center" colspan="4">Sutaupyta</th>
                    </tr>

                    <tr>
                        <th>Iš viso</th>
                        <td class="{{ $totalDiff < 0 ? 'table-danger' : 'table-primary' }} text-right">{{ number_format($totalDiff, 2) }}</td>
                        <td class="text-right">{{ number_format($totalBudgetDiff, 2) }}</td>
                        <td class="text-right {{ ($totalDiff - $totalBudgetDiff) < 0 ? 'table-danger' : 'table-success' }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="{{ $totalBudgetDiff !== 0 ? round(($totalDiff - $totalBudgetDiff) * 100 / $totalBudgetDiff, 2) : 0 }}%">{{ number_format(($totalDiff - $totalBudgetDiff), 2) }}</td>
                    </tr>

                    </tbody>

                </table>


            </div>
        </div>
    </div>

@endsection
