@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="jumbotron text-center">
                <h1 class="display-4">Išlaidos</h1>
                <p class="lead">Pajamų ir išlaidų skaičiuoklė. Asmeninio biudžeto planavimas</p>
                <hr class="my-4">
                <p>Ši skaičiuoklė padės jums planuoti savo pajamas ir išlaidas. Susidarykite savo asmenines pajamų ir
                    išlaidų kategorijas ir sekite, kur išleidžiate daugiausiai. Išlaidų sekimas yra pirmas žingsnis
                    pradedant taupyti.</p>
                <p class="lead">
                    <a class="btn btn-dark btn-lg" href="{{ route('register') }}" role="button">Registruotis</a>
                </p>
            </div>
        </div>
    </div>
@endsection
