/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('chart', require('./components/chart.vue'));

const app = new Vue({
    el: '#app'
});

$('input[name^="earnings"], input[name^="expenses"], input[name^="sum"]').on('input', function () {
    let sum = $(this).val();
    $(this).val(sum.replace(/([^0-9,.])/g, "").replace(",", "."));
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$(function () {
    $('[data-toggle="popover"]').popover()
})

$(function () {
    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        firstDay: 1
    })
});