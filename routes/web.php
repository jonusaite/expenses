<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::guest()) {
        return view('welcome');
    } else {
        return redirect()->route('home');
    }
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
    Route::resource('earnings', 'EarningController', [
        'except' => [
            'show',
        ],
    ]);

    Route::resource('earnings_categories', 'EarningsCategoryController', [
        'except' => [
            'show',
        ],
    ]);
    Route::resource('expenses', 'ExpenseController', [
        'except' => [
            'show',
        ],
    ]);

    Route::resource('expenses_categories', 'ExpensesCategoryController', [
        'except' => [
            'show',
        ],
    ]);

    Route::get('/balance', 'BalanceController@index')->name('balance.index');

    Route::get('/budget', 'BudgetController@index')->name('budget.index');

    Route::get('/budget/create', 'BudgetController@create')->name('budget.create');
    Route::middleware('date')->group(function () {
        Route::get('/budget/{year}/{month}/edit', 'BudgetController@edit')->name('budget.edit');
        Route::put('/budget/{year}/{month}', 'BudgetController@update')->name('budget.update');
        Route::get('/budget/{year}/{month}/confirm', 'BudgetController@confirm')->name('budget.confirm');
        Route::delete('/budget/{year}/{month}', 'BudgetController@destroy')->name('budget.destroy');

    });
    Route::post('/budget', 'BudgetController@store')->name('budget.store');

    Route::get('/overview', 'BalanceController@overview');

});
