System for calculating expenses and budget.

Live working example:
http://islaidos.kuriu.eu/

1. Download files from repo.
2. Install and configure Laravel: https://laravel.com/docs/5.5
3. Migrate databases. Instructions: https://laravel.com/docs/5.5/migrations

Copyright (c) 2017-2019 Aistė Jonušaitė